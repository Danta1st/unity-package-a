﻿using UnityEngine;

namespace UnityPackageA
{
    public class MyPackageLogger
    {
        private const string Version = "0.1.1";
        public static void Log(string message)
        {
            var prefix = typeof(MyPackageLogger).Namespace;
            var mes = string.Format("{0} [{1}] {2}",Version, prefix, message);
            
            Debug.Log(mes);
        }
    }
}